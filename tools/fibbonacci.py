def calculate_fibb_level_price(price_a, price_b, desired_level, side="Short"):
    if side == "Short":
        diff = abs(price_b - price_a)
        return round(price_a + (diff * desired_level))
    else:
        diff = abs(price_b - price_a)
        return round(price_a - (diff * desired_level))


def my_round(x, prec=2, base=.5):
    return round(base * round(float(x) / base), prec)
