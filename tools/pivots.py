import numpy as np


#
# Copyright Josiah Kendall 2019
#
# Calculate the pivot highs and the pivot lows of a data set.
#
def calulate_pivots_bitmex(data, data_range):

    last_high = -1
    last_low = -1
    last_low_index = 0
    last_high_index = 0
    lows = []
    highs = []
    for d in data:
        lows.append(d["low"])
        highs.append(d["high"])
    lows = np.array(lows)
    highs = np.array(highs)

    # create array of each each side
    center = data_range - 1
    left = 0
    right = (data_range + data_range) - 1

    pivot_highs = []
    pivot_lows = []
    while right < len(lows) - 1:
        # Change this to be a different low
        current = data[center]
        relevant_low = lows[left:right]
        if current["low"] <= relevant_low.min():
            if current["low"] != last_low or last_low_index < left:
                pivot_lows.append(current)
                last_low = current["low"]
                last_low_index = center

        relevant_high = highs[left:right]
        if current["high"] >= relevant_high.max():
            if current["high"] != last_high or last_high_index < left:
                pivot_highs.append(current)
                last_high = current["high"]
                last_high_index = center

        center += 1
        right += 1
        left += 1
    return np.array([pivot_highs, pivot_lows])