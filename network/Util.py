import urllib.request as urllib2
import datetime
import json


def get_json(url):
    response = urllib2.urlopen(url)
    return json.load(response)


class Util:

    def __init__(self):
        pass
