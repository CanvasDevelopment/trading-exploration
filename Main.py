import random as random
import network
from bitmex import BitmexApi, Downloader
from bitmex.BitmexDatasource import BitmexDatasource
from indicators.moving_averages import sma
from indicators.oscilators import rsi
from tools import pivots, fibbonacci
from strategies import Sweetspot


if __name__ == '__main__':
    # Download the data
    # downloader = Do
    # Downloader.download_data()
    # api = BitmexApi.BitmexApi()
    timeUnit = "m"
    amount = 5
    # data = api.downloadData(amount, timeUnit, 1000, 0)

    print("Beginning backtest")
    print("Timeframe : {}{}".format(amount, timeUnit))

    ss = Sweetspot.SweetSpot()
    ss.calculateTradePotential()
