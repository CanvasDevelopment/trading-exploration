import network.Util


class BitmexApi:

    def __init__(self):
        self.baseUrl = "https://www.bitmex.com/api/v1"
        self.url = ""

    # Download data for a timeframe.
    # Set the timeframe as a single decimal. E,g 1 for 1 minute, 5, for 5 minutes etc
    def downloadData(self, time_frame, unit, count, start):
        self.url = "https://www.bitmex.com/api/v1/trade/bucketed?binSize={}{}&partial=false&symbol=XBT&count={" \
                   "}&start={}&" \
                   "reverse=true".format(time_frame, unit, count, start)
        data = network.Util.get_json(self.url)
        return data
