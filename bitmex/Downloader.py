from bitmex import BitmexApi
from database import DB
from database import DB

def download_data():

    db = DB.DB()
    api = BitmexApi.BitmexApi()
    timeframe = 60
    counter = 0
    while counter < 100:

        # 1 hour - needs to be changed to 60 mins here todo
        data = api.downloadData(1, "h", 1000, counter * 1000)

        for frame in data:
            db.save_bitmex_data_frame(frame, timeframe)

        print("data timestamp is {}".format(data[0]["timestamp"]))

        counter += 1

