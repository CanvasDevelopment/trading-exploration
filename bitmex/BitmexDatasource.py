class BitmexDatasource:

    def __init__(self, data):
        self.data = data
        self.close = 'close'
        self.open = 'open'
        self.high = 'high'
        self.low = 'low'

    def produce_closes(self):
        closes = []
        for d in self.data:
            closes.append(d[self.close])
        return closes

