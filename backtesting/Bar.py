from dataclasses import dataclass


@dataclass
class Bar:
    open: float
    close: float
    high: float
    low: float
