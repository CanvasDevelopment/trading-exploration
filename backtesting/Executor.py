'''
    @author Josiah Kendall
'''

class Executor:

    """
    The constructor for the executor. This takes in two parameters.

    :arg bts        This is the [BacktestSource] which can be used to pull out data
                    in a way that simulates a live environment.

    :arg strategy   This is the strategy that defines how we interact with the data
                    in regards to our trades. This creates the [Trade] objects, which
                    get recorded here in our executor.
    """
    def __init__(self, bts, strategy):
        self.bts = bts
        self.strategy = strategy
        self.active_trades = []

    ''' Execute the back test '''
    def execute(self):
        has_data = 0
        while has_data == 0:
            # bar = get_next_bar
            # new_trade = strategy.apply(bar)
            # if new_trade != null
            # active_trades.append(new_trade)
            #