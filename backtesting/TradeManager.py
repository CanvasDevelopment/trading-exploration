from backtesting import Trade
from backtesting.Bar import Bar


class TradeManager:
    LONG = "Long"
    SHORT = "Short"

    # Set up our trade lists
    def __init__(self):

        # All active trades that have been triggered
        self.active_trades = []

        # Trades that have been placed but are not yet active
        self.orders = []

        self.RR = 0
        self.RRStates = []

        # Losers
        self.stopped_trades = []

        # Winners
        self.successful_trades = []

    def add_new_order(self, order: Trade):

        for activeTrade in self.active_trades:
            if activeTrade.side == order.side and activeTrade.entry == order.entry:
                return

        for stoppedTrade in self.stopped_trades:
            if stoppedTrade.side == order.side and stoppedTrade.entry == order.entry:
                return

        for pre_active_trade in self.orders:

            if order.entry == pre_active_trade.entry and order.side == pre_active_trade.side:
                self.orders.remove(pre_active_trade)

        self.orders.append(order)

    def process_active_orders(self, price):
        for trade in self.orders:
            # Activate trades ( LONG and SHORT )
            if (trade.side == self.LONG and trade.entry >= price) \
                    or (trade.side == self.SHORT and trade.entry <= price):
                self.active_trades.append(trade)

            if (trade.side == self.LONG and trade.pivot < price) \
                    or (trade.side == self.SHORT and trade.pivot > price):
                self.orders.remove(trade)

        for trade in self.active_trades:
            if self.orders.__contains__(trade):
                self.orders.remove(trade)

    def process_active_trades(self, price):
        for trade in self.active_trades:
            # Ensure that we trigger profit taking
            if (trade.side == self.LONG and trade.tp1 <= price) \
                    or (trade.side == self.SHORT and trade.tp1 >= price):
                self.successful_trades.append(trade)
                stopSpace = abs(trade.stop - trade.entry)
                profitSpace = abs(trade.entry - trade.tp1)
                rr = profitSpace / stopSpace
                self.RR += rr
                totalTrades = len(self.successful_trades) + len(self.stopped_trades)
                winningTrades = len(self.successful_trades)

                print("Performance : {}. Win Rate : {}".format(self.RR, winningTrades / totalTrades))
                self.RRStates.append(self.RR)

            # Ensure that we trigger stops too
            if (trade.side == self.LONG and trade.stop >= price) \
                    or (trade.side == self.SHORT and trade.stop <= price):
                self.stopped_trades.append(trade)
                self.RR -= 1
                totalTrades = len(self.successful_trades) + len(self.stopped_trades)
                winningTrades = len(self.successful_trades)

                print("Performance : {}. Win Rate : {}".format(self.RR, winningTrades / totalTrades))
                self.RRStates.append(self.RR)

        for trade in self.stopped_trades:
            if self.active_trades.__contains__(trade):
                self.active_trades.remove(trade)

        for trade in self.successful_trades:
            if self.active_trades.__contains__(trade):
                self.active_trades.remove(trade)

    def handle_new_bar(self, bar: Bar):
        self.process_active_orders(bar.high)
        self.process_active_orders(bar.low)
        self.process_active_trades(bar.high)
        self.process_active_trades(bar.low)
