import unittest
from backtesting import TradeManager
from backtesting import Trade
from backtesting.Bar import Bar


class MyTestCase(unittest.TestCase):

    def test_ensureThatWeCanInsertInCorrectPlace(self):
        tradeManager = TradeManager.TradeManager()

        trade1 = Trade.Trade(10.0, 12.0, 8.0, 7.0, 6.0, "Short")
        trade2 = Trade.Trade(10.0, 12.0, 8.0, 7.0, 6.0, "Long")
        trade3 = Trade.Trade(11.0, 12.0, 8.0, 7.0, 6.0, "Long")
        trade4 = Trade.Trade(10.0, 12.0, 8.0, 7.0, 6.0, "Short")

        tradeManager.add_new_order(trade1)
        tradeManager.add_new_order(trade2)
        tradeManager.add_new_order(trade3)
        tradeManager.add_new_order(trade4)

        self.assertEqual(len(tradeManager.orders), 3)
        self.assertEqual(tradeManager.orders[2], trade4)

    ''' Test that we can identify when to activate trades correctly'''
    def test_weCanActivateOrdersIntoActiveTrades(self):
        trade_manager = TradeManager.TradeManager()

        trade1 = Trade.Trade(10.0, 12.0, 8.0, 7.0, 6.0, "Short")
        trade2 = Trade.Trade(10.0, 12.0, 8.0, 7.0, 6.0, "Long", 12.0)
        trade3 = Trade.Trade(11.0, 12.0, 8.0, 7.0, 6.0, "Long",12.0)

        trade_manager.add_new_order(trade1)
        trade_manager.add_new_order(trade2)
        trade_manager.add_new_order(trade3)

        trade_manager.process_active_orders(13)
        self.assertEqual(len(trade_manager.active_trades), 1)
        self.assertEqual(len(trade_manager.orders), 2)

        trade_manager.process_active_orders(12)
        self.assertEqual(len(trade_manager.active_trades), 1)
        self.assertEqual(len(trade_manager.orders), 2)

        trade_manager.process_active_orders(11)
        self.assertEqual(len(trade_manager.active_trades), 2)
        self.assertEqual(len(trade_manager.orders), 1)

        trade_manager.process_active_orders(9)
        self.assertEqual(len(trade_manager.active_trades), 3)
        self.assertEqual(len(trade_manager.orders), 0)

    ''' Test that we can identify when to activate trades correctly'''

    def test_weCanStopTrades(self):
        trade_manager = TradeManager.TradeManager()

        trade1 = Trade.Trade(10.0, 12.0, 8.0, 7.0, 6.0, "Short", 12.0)
        trade2 = Trade.Trade(10.0, 8.0, 12.0, 7.0, 6.0, "Long", 8.0)
        trade3 = Trade.Trade(9.0, 8.0, 15.0, 7.0, 6.0, "Long", 8.0)

        trade_manager.add_new_order(trade1)
        trade_manager.add_new_order(trade2)
        trade_manager.add_new_order(trade3)

        trade_manager.process_active_orders(11)
        trade_manager.process_active_trades(11)

        self.assertEqual(len(trade_manager.active_trades), 1)

        trade_manager.process_active_orders(11.5)
        trade_manager.process_active_trades(11.5)

        self.assertEqual(len(trade_manager.active_trades), 1)
        self.assertEqual(len(trade_manager.stopped_trades), 0)

        trade_manager.process_active_orders(12)
        trade_manager.process_active_trades(12)

        self.assertEqual(len(trade_manager.active_trades), 0)
        self.assertEqual(len(trade_manager.stopped_trades), 1)

    ''' Test that we can identify when to activate trades correctly'''

    def test_weCanTakeProfit(self):
        trade_manager = TradeManager.TradeManager()

        trade1 = Trade.Trade(10.0, 12.0, 8.0, 7.0, 6.0, trade_manager.SHORT,12)
        trade2 = Trade.Trade(10.0, 8.0, 12.0, 7.0, 6.0, trade_manager.LONG, 8.0)
        trade3 = Trade.Trade(9.0, 7.0, 15.0, 7.0, 6.0, trade_manager.LONG, 7.0)

        trade_manager.add_new_order(trade1)
        trade_manager.add_new_order(trade2)
        trade_manager.add_new_order(trade3)

        trade_manager.process_active_orders(11)
        trade_manager.process_active_trades(11)

        self.assertEqual(len(trade_manager.active_trades), 1)

        trade_manager.process_active_orders(8)
        trade_manager.process_active_trades(8)

        self.assertEqual(len(trade_manager.active_trades), 1)
        self.assertEqual(len(trade_manager.stopped_trades), 1)
        self.assertEqual(len(trade_manager.successful_trades), 1)

    def test_weCanProcessBarThatStopsOurLong(self):
        bar1 = Bar(63.5, 65.5, 66, 63.5)
        bar2 = Bar(65.5, 67.5, 69, 63)
        bar3 = Bar(67.5, 63.5, 69, 63.5)
        bar4 = Bar(63.5, 54.5, 63.5, 52)

        trade_manager = TradeManager.TradeManager()

        trade1 = Trade.Trade(63, 55, 70, 0, 0, trade_manager.LONG, 55)
        trade_manager.add_new_order(trade1)

        trade_manager.handle_new_bar(bar1)
        self.assertEqual(len(trade_manager.active_trades), 0)
        trade_manager.handle_new_bar(bar2)
        self.assertEqual(len(trade_manager.active_trades), 1)
        trade_manager.handle_new_bar(bar3)
        self.assertEqual(len(trade_manager.active_trades), 1)
        trade_manager.handle_new_bar(bar4)
        self.assertEqual(len(trade_manager.active_trades), 0)
        self.assertEqual(len(trade_manager.stopped_trades), 1)


    def test_weWillDestroyShortTradesWhenTheCurrentLowIsLowerThanTheBottomPivot(self):
        trade_manager = TradeManager.TradeManager()

        trade1 = Trade.Trade(63, 55, 70, 0, 0, trade_manager.LONG, 90)
        trade_manager.add_new_order(trade1)

        bar1 = Bar(63.5, 65.5, 66, 63.5)
        bar2 = Bar(63.5, 65.5, 66, 63.5)
        bar3 = Bar(63.5, 65.5, 66, 63.5)
        bar4 = Bar(63.5, 93.5, 96, 63.5)

        trade_manager.handle_new_bar(bar1)
        trade_manager.handle_new_bar(bar2)
        trade_manager.handle_new_bar(bar3)
        self.assertEqual(len(trade_manager.orders), 1)

        trade_manager.handle_new_bar(bar4)

        self.assertEqual(len(trade_manager.orders), 0)

    def test_weWillDestroyLongTradesWhenTheCurrentHighIsHigherThanTheTopPivot(self):
        trade_manager = TradeManager.TradeManager()

        trade1 = Trade.Trade(63, 55, 70, 0, 0, trade_manager.LONG, 90)
        trade_manager.add_new_order(trade1)

        bar1 = Bar(63.5, 65.5, 66, 63.5)
        bar2 = Bar(63.5, 65.5, 66, 63.5)
        bar3 = Bar(63.5, 65.5, 66, 63.5)
        bar4 = Bar(63.5, 93.5, 96, 63.5)

        trade_manager.handle_new_bar(bar1)
        trade_manager.handle_new_bar(bar2)
        trade_manager.handle_new_bar(bar3)
        self.assertEqual(len(trade_manager.orders), 1)

        trade_manager.handle_new_bar(bar4)

        self.assertEqual(len(trade_manager.orders), 0)


if __name__ == '__main__':
    unittest.main()
