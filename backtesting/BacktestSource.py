class BacktestSource:
    def __init__(self, data):
        self.liveIndex = 0
        print("initialised backtest source")

    def get_next_bar(self):
        bar = self.data[self.liveIndex]
        self.liveIndex += 1
        return bar