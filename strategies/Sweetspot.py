from tools import pivots, fibbonacci
from database import DB

from backtesting import TradeManager
from backtesting import Trade
from backtesting import Bar
import math


class SweetSpot:
    tradeManager = TradeManager.TradeManager()

    def __init__(self):
        self.open = 0
        self.close = 1
        self.high = 2
        self.low = 3
        self.db = DB.DB()
        self.data = []
        self.openTrades = []
        self.executedTrades = []
        self.openOrders = []

    def fetchData(self, order, offset, timeframe):
        data = self.db.fetchData(order, offset, timeframe)
        count = 0
        for d in data:
            d["index"] = len(data) - count
            count += 1
        return data

    def calculateTradePotential(self):
        number_of_datapoints = self.db.fetch_total_count(60)
        data = self.fetchData("ASC", 0, 5)

        # pivots = data[]
        count = 0
        while count <= number_of_datapoints:
            ref = count % 1000

            if ref == 0:
                data = self.fetchData("ASC", count, 5)

            snapshot = data[len(data) - ref:]

            # produce pivots (high and low)
            pivot_points = pivots.calulate_pivots_bitmex(snapshot, 16)

            highs = pivot_points[0]
            lows = pivot_points[1]

            count += 1
            if len(snapshot) > 0:
                latest = snapshot[0]
                bar = Bar.Bar(latest["open"], latest["close"], latest["high"], latest["low"])
                self.tradeManager.handle_new_bar(bar)
            if len(lows) == 0:
                continue

            if len(highs) == 0:
                continue

            # Get most recent pivot
            recent_low = lows[0]

            recent_high = highs[0]

            lowest = self.get_lowest_from_now_until_recentHigh(recent_high, snapshot)
            highest = self.get_highest_from_now_until_recent_low(recent_low, snapshot)

            previous_low = self.find_previous_low(recent_high, lows)
            previous_high = self.find_previous_high(recent_low, highs)

            stop = fibbonacci.calculate_fibb_level_price(lowest, recent_high["high"], 0.66)
            golden = fibbonacci.calculate_fibb_level_price(lowest, recent_high["high"], 0.618)
            fifty = fibbonacci.calculate_fibb_level_price(lowest, recent_high["high"], 0.55)

            bearTargetDiff = abs(previous_low['low'] - lowest)

            if fifty < previous_low["low"] < golden:
                order = Trade.Trade(previous_low['low']-(bearTargetDiff * 0.04), stop, lowest+(bearTargetDiff * 0.75), lowest, lowest, self.tradeManager.SHORT, lowest)
                self.tradeManager.add_new_order(order)

            stop = fibbonacci.calculate_fibb_level_price(highest, recent_low["low"], 0.66, "LONG")
            golden = fibbonacci.calculate_fibb_level_price(highest, recent_low["low"], 0.618, "LONG")
            fifty = fibbonacci.calculate_fibb_level_price(highest, recent_low["low"], 0.55, "LONG")

            bullTargetDiff = abs(previous_high['high'] - highest)

            if golden < previous_high["high"] < fifty:
                order = Trade.Trade(previous_high["high"] + (bullTargetDiff * 0.04), stop, highest-(bullTargetDiff*0.75), highest, highest, self.tradeManager.LONG,
                                    highest)
                self.tradeManager.add_new_order(order)

        print("Number of Bars : {}".format(number_of_datapoints))
        totalTrades = len(self.tradeManager.successful_trades) + len(self.tradeManager.stopped_trades)
        winningTrades = len(self.tradeManager.successful_trades)
        print("Win Rate : {}".format(winningTrades / totalTrades))
        print("Wins : {} | Losses : {}".format(len(self.tradeManager.successful_trades),
                                               len(self.tradeManager.stopped_trades)))

        print("losers: {}".format(self.tradeManager.stopped_trades))
        print("winners : {}".format(self.tradeManager.successful_trades))
        print("orders : {}".format(self.tradeManager.orders))
        print("active : {}".format(self.tradeManager.active_trades))

        print("Total R:R is : {}".format(self.calculateRR()))

    def calculateRR(self):
        total_rr = 0
        for trade in self.tradeManager.successful_trades:
            stopSpace = abs(trade.stop - trade.entry)
            profitSpace = abs(trade.entry - trade.tp1)

            total_rr += profitSpace / stopSpace
        for losingTrade in self.tradeManager.stopped_trades:
            total_rr -= 1

        return total_rr

    def get_lowest_from_now_until_recentHigh(self, high, snapshot):
        lowest = high["high"]
        for d in snapshot:
            if d["index"] <= high["index"]:
                return lowest

            if lowest > d["low"]:
                lowest = d["low"]

        return lowest

    def get_highest_from_now_until_recent_low(self, low, snapshot):
        highest = low["low"]
        for d in snapshot:
            if d["index"] <= low["index"]:
                return highest

            if highest < d["high"]:
                highest = d["high"]

        return highest

    def find_previous_low(self, recent_high, lows):

        for low in lows:
            if recent_high["index"] >= low["index"]:
                return low
        return recent_high

    def find_previous_high(self, recent_low, highs):

        for high in highs:
            if recent_low["index"] >= high["index"]:
                return high
        return recent_low
