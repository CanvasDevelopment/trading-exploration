import mysql.connector


class DB:

    def __init__(self):
        self.my_db = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="Idnw2bh2!7873",
            database="bitmex_data"
        )

    def save_bitmex_data_frame(self, data, time_frame):
        my_cursor = self.my_db.cursor()

        sql = "INSERT INTO datasource (" \
              "timeframe, " \
              "timestamp," \
              "symbol," \
              "open," \
              "high," \
              "low," \
              "close," \
              "trades," \
              "volume," \
              "vwap," \
              "lastSize ) VALUES (%s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s )"
        val = (
            time_frame,
            data["timestamp"],
            data["symbol"],
            data["open"],
            data["high"],
            data["low"],
            data["close"],
            data["trades"],
            data["volume"],
            data["vwap"],
            data["lastSize"]
        )
        my_cursor.execute(sql, val)
        self.my_db.commit()


    def fetchData(self, order, offset, timeframe):
        data = []
        sql = "select open, close, high, low from datasource where timeframe = {} order by timestamp asc limit 1000 " \
              "offset {}".format(timeframe, offset)
        cursor = self.my_db.cursor()
        cursor.execute(sql)
        result = cursor.fetchall()
        for row in result:
            dataObject = {}
            dataObject["open"] = row[0]
            dataObject["close"] = row[1]
            dataObject["high"] = row[2]
            dataObject["low"] = row[3]
            data.insert(0, dataObject)
        return data



    def fetch_trade(self, trade_id):
        pass

    def fetch_total_count(self, timeframe):
        sql = "select count(id) from datasource where timeframe = {}".format(timeframe)
        cursor = self.my_db.cursor()
        cursor.execute(sql)
        result = cursor.fetchone()
        return result[0]