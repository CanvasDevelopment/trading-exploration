import numpy as np

'''
    @author Josiah kendall
    copyright 2020
'''


# Calculate down days and up days
# Average the down moves and the up moves
# calculate Relative Strength (RS = Relative Strength = AvgU / AvgD)
# see https://www.macroption.com/rsi-calculation/ for more detail on the calculation
def calculate_rsi(data, period):
    rsi = []
    base = 0
    while base < len(data) - 1:
        section = data[base: base + period + 1]
        rsi.append(calculate_specific_rsi_point(section))
        base += 1

    return rsi


def calculate_specific_rsi_point(data):
    down_days = []
    up_days = []

    size = len(data)-1
    # data = data[::-1]
    while size > 0:

        closet = data[size - 1]
        closet1 = data[size]
        size -= 1

        difference = closet - closet1

        # If we have above zero, add our difference, else add 0
        if difference > 0:
            up_days.append(difference)
            down_days.append(0)
        else:
            up_days.append(0)
            down_days.append(abs(difference))

    period = len(data) - 1
    alpha = float(2) / float((period + 1))

    up_days_avg = alpha * up_days[period - 1] + (1 - alpha) * np.average(up_days[:period - 1])
    down_days_avg = alpha * down_days[period - 1] + (1 - alpha) * np.average(down_days[:period - 1])

    rs = np.average(up_days) / np.average(down_days)
    return 100 - 100 / (1 + rs)


# smoothed moving average
# for details plz refer to wikipedia
# https://en.wikipedia.org/wiki/Moving_average#Modified_moving_average
def smma(series, n):
    output = [series[0]]

    for i in range(1, len(series)):
        temp = output[-1] * (n - 1) + series[i]
        output.append(temp / n)

    return output


# In[3]:

# calculating rsi is very simple
# except there are several versions of moving average for rsi
# simple moving average, exponentially weighted moving average, etc
# in this script, we use smoothed moving average(the authentic way)
def rsi2(data, n=14):
    delta = data.diff().dropna()

    up = np.where(delta > 0, delta, 0)
    down = np.where(delta < 0, -delta, 0)

    rs = np.divide(smma(up, n), smma(down, n))

    output = 100 - 100 / (1 + rs)

    return output[n - 1:]

