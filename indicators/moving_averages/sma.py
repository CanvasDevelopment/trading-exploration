import numpy as np
'''
    @author Josiah kendall
    copyright 2019
'''


# Calculate the moving average of a data set
def calculate_sma(list_of_closes, period):
    averages = []
    head = period
    base = 0
    while head < len(list_of_closes)-1:
        section = list_of_closes[base: head]
        averages.append(np.average(section))
        head += 1
        base += 1

    return averages


